FROM openjdk:17
EXPOSE 8080
ADD target/ems-docker-jen.jar ems-docker-jen.jar 
ENTRYPOINT ["java","-jar","ems-docker-jen.jar"]