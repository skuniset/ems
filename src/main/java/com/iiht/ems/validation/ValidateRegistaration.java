package com.iiht.ems.validation;

import java.util.Arrays;
import java.util.List;

import com.iiht.ems.model.Registration;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class ValidateRegistaration implements ConstraintValidator<RegistrationValidation, Registration>{



	@Override
	public boolean isValid(Registration registration, ConstraintValidatorContext context) {
		
		boolean isValid = false;
		List<String> courses = Arrays.asList("JAVA","C");
		if (courses.contains(registration.getCourse())) {
			isValid=  true;
		}
		return isValid;
	
	}

}
