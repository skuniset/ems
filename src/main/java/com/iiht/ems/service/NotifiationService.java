package com.iiht.ems.service;

import java.net.URISyntaxException;

import com.iiht.ems.dto.CourseRegistrationDto;

public interface NotifiationService {

	public boolean sendRegistrationNotification(CourseRegistrationDto registraationDto) throws URISyntaxException;
}
