package com.iiht.ems.dto;

//import org.hibernate.envers.Audited;

import com.iiht.ems.validation.RegistrationValidation;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
//@Audited
public class CourseRegistrationDto extends BaseDto {

	private String email;
	private String name;
	@RegistrationValidation
	private String course;
	
}
