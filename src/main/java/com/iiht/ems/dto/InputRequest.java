package com.iiht.ems.dto;

import lombok.Data;

@Data
public class InputRequest<T> {

	private String user;
	private T classType ;
}
