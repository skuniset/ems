package com.iiht.ems.model;

import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/*@IdClass(CompositePrimaryKey.class)*/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table
@Entity
public class PkRegistration{

	/*
	 * this is with out @Embedded Annnotation
	 * @Id private Long id;
	 * 
	 * @Id private String email;
	 */
	
	@EmbeddedId
	private CompositePrimaryKey compositePrimaryKey;
	
	private String name;
	
}
